#!/usr/bin/env bash
update () {
  name="$(echo "$@" | jq -r '.container.name')"
  id="$(echo "$@" | jq -r '.container.id')"
  if [[ "$oldid" != "$id" ]]; then
    oldid="$id"
    if [[ "$name" != "null" ]]; then
      echo "{\"text\": \"$name\", \"alt\": \"title\", \"class\": \"windowtitle\", \"tooltip\": \"<b>Window Title</b>\"}"
    fi
  else
    if [[ "$oldname" != "$name" ]]; then
      oldname="$name"
      if [[ "$name" != "null" ]]; then
        sleep 0.2
        echo "{\"text\": \"$name\", \"alt\": \"title\", \"class\": \"windowtitle\", \"tooltip\": \"<b>Window Title</b>\"}"
      fi
    fi
  fi
}
swaymsg -t subscribe -m '["window"]' | while read -r line; do update "$line"; done


