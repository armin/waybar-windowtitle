### !?

windowtitle.sh should go into:
~/.config/waybar/custom_modules/windowtitle.sh

Configuration in ~/.config/waybar/config:

1) Create stanza like

    "custom/windowtitle": { 
      "format": "{icon} {}",
      "interval": "2",   
      "return-type": "json",
      "exec": "$HOME/.config/waybar/custom_modules/windowtitle.sh",
        "format-icons": {
            "windowtitle": "",
        },
    },

2) Put "custom/windowtitle" into modules-left, modules-right, modules-center or so.

3) done.

### LICENSE

This software is released under the terms of the WTFPL (http://www.wtfpl.net/).


